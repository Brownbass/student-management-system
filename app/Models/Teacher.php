<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public $fillable = [
        'first_name',
        'last_name'
    ];



    public function Groups()
    
    {
        return $this->hasMany('App\Models\Group');
    }

}
