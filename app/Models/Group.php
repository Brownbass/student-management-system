<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function Teacher()
    
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    public function Students()
    
    {
        return $this->hasMany('App\Models\Student');
    }


}
