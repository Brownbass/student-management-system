<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



// List students
Route::get('students', 'StudentController@index');
// List single student
Route::get('student/{id}', 'StudentController@show');
// Create new student
Route::post('student', 'StudentController@store');
// Update student
Route::put('student', 'StudentController@store');
// Delete student
Route::delete('student/{id}', 'StudentController@destroy');

Route::get('groups', 'GroupController@index');