<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Group::class, function (Faker $faker) {
    return [
        'name' => "Class " . $faker->colorName,
        'teacher_id' => factory('App\Models\Teacher')->create()->id,
    ];
});
