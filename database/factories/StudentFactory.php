<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Student::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name'=> $faker->lastName,
        'group_id' => factory('App\Models\Group')->create()->id,
    ];
});
